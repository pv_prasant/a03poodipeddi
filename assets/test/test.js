// This file tests our application code - open this file in a browser to see the test results. 

// It uses QUnit, a unit testing library for JavaScript. 

// Unit testing is a common, professional practice. 

// It helps us verify our code is correct in all cases.

// To complete this assignment, go to the end of this file, and uncomment the last 5 tests. 

// Refresh.  You will see several failing tests. 

// Modify the code in your appplication (M03.js) to get these tests to pass. 

// References: 
// https://qunitjs.com/
// https://www.sitepoint.com/getting-started-qunit/
// http://jsbin.com/tusuvixi/1/edit?html,js,output


QUnit.test('Testing the new add function with four sets of inputs', function (assert) {
    assert.equal(App.sum(2, 2), 4, "works with two positive integers");
    assert.equal(App.sum(-3, -3, -2), -8, "works with three negative integers");
    assert.equal(App.sum(2.5, 2.5, 2.5, 2.5), 10, "works with four positive real numbers");
    assert.equal(App.sum(10, -10), 0, "works with a positive and a negative");
});





