var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var http = require('http').Server(app);

// set up the view engine
app.use(express.static(__dirname + '/views'));
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname+ '/assets/')));
app.get("/", function (request, response)
{
  response.sendFile(path.join(__dirname+'/assets/index.html'));
});

// http GET (default and /new-entry)
app.get("/guestbook", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/contact",function(request, response){
  response.render("contact");
});

// http POST (INSERT)
app.post("/contact",function(request,response){
var api_key = 'key-071f974b292924ef7d4b9155be792512';
var domain = 'sandbox801e3a70bfe54d8aaa67a053b649977c.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
var data = {
  from: 'No Reply<postmaster@sandbox801e3a70bfe54d8aaa67a053b649977c.mailgun.org>',
  to: 'S528167@mail.nwmissouri.edu',
  subject: request.body.name,
  text: request.body.message
};
 
mailgun.messages().send(data, function (error, body) {
  console.log(body);
  if (!error) {
    response.send("Mail Sent");
    }
  else
  {
    response.send("Mail not sent");
  }
});
});
app.post("/new-entry", function(request, response){
  if(!request.body.title||!request.body.body){
    response.status(400).send("entries must have a title and a body.");
    return;
  
}
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/");
});

// 404
app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
